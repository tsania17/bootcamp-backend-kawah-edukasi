package org.tsania;

// mengimport package Toko yang berisikan deklarasi atribut
import org.tsania.models.Toko;

// mengimport package untuk anotasi GET, POST, MediaType, dsb
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

// mengimport package java untuk tipe objek yang digunakan (menggunakan map karena sudah include key yang dijadikan sbg id)
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

// mengimport package untuk validasi 
import javax.validation.Valid;
import javax.inject.Inject;
import javax.validation.Validator;
import javax.validation.ConstraintViolation;

@Path("toko")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TokoResource {

    // menginisialisasi objek barang dengan menggunakan map
    public Map<Integer, Toko> barangToko = new HashMap<>();
    // menginisialisasi id bertipe integer yang dimulai angka 1, sebagai key dari objek barang
    public Integer id = 1;

    @Inject
    Validator validator;

    // membuat konstruktor yang akan langsung menginisialisasi default nilai tiap atribut (agar tiap restart apk tidak kosong isinya)
    public TokoResource(){
        Toko barang1 = new Toko("Buku Tulis", "Buku tulis kertas putih isi 50 lembar", "Sinar Dunia", 4000);
        Toko barang2 = new Toko("Pensil", "Pensil kayu raut warna hitam", "Faber Castell", 3000);
        barangToko.put(id++, barang1);
        barangToko.put(id++, barang2);
    }

    // fungsi untuk menampilkan semua barang di toko
    @GET
    public Map<Integer, Toko> semuaBarangToko() {
        return barangToko;
    }

    // fungsi untuk menampilkan barang sesuai id yang dikehendaki
    @GET
    @Path("{id}")
    public Toko barangToko(@PathParam("id") Integer id) {
        return barangToko.get(id);
    }

    // fungsi untuk menambah barang
    @POST
    public Result tambahBarang(Toko barang) {
        Set<ConstraintViolation<Toko>> violations = validator.validate(barang);
        if (violations.isEmpty()) {
            barangToko.put(id++, barang);
            return new Result("Barang berhasil ditambahkan");
        } else {
            return new Result(violations);
        }
    }

    // fungsi untuk update atau mengedit barang yang telah ada
    @PUT 
    @Path("{id}")
    public Object editBarang(@PathParam("id") Integer id, @Valid Toko barang) {
        Map<String, Toko> baru = new HashMap<>();
        baru.put("Barang semula", barangToko.get(id));
        barangToko.replace(id, barang);
        baru.put("Barang pengganti", barangToko.get(id));
        return baru;
    }

    // fungsi untuk menghapus suatu barang berdasar id tertentu
    @DELETE
    @Path("{id}")
    public Toko hapusBarang(@PathParam("id") Integer id) {
        Toko barang = barangToko.get(id);
        barangToko.remove(id);
        return barang;
    }

    // fungsi untuk menghapus seluruh barang yang ada di toko
    @DELETE
    @Produces(MediaType.TEXT_PLAIN)
    public String hapusSemuaBarang() {
        barangToko.clear();
        // menginisialisasi id agar kembali bernilai 1
        id = 1;
        return "Semua barang berhasil dihapus..";
    }

    public static class Result {
        Result(String message) {
            this.success = true;
            this.message = message;
        }
        Result(Set<? extends ConstraintViolation<?>> violations) {
            this.success = false;
            this.message = violations.stream()
            .map(cv -> cv.getMessage())
            .collect(Collectors.joining(", "));
        }
        private String message;
        private boolean success;
        public String getMessage() {
            return message;
        }
        public boolean isSuccess() {
            return success;
        }
    }
}