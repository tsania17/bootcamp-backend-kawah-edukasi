// package file 
package org.tsania.models;

// mengimport package untuk validasi atribut
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Min;

public class Toko {

    // mendeskripsikan tipe data atribut beserta validasi masing-masing atribut
    // Validasi => Not blank    : tidak boleh data kosong (untuk String)
    // Validasi => Not null    : tidak boleh data kosong (untuk Integer)
    // Validasi => Min          : minimal nilai yang bisa diisikan

    @NotBlank(message = "Masukkan nama item barang")
    public String item;
    @NotBlank(message = "Masukkan deskripsi barang")
    public String deskripsi;
    @NotBlank(message = "Masukkan merek item barang")
    public String merek;
    @NotNull(message = "Masukkan harga barang")
    @Min(value = 0, message = "Harga barang minimal 0 rupiah")
    public Integer harga;

    //membuat konstruktor dari class Toko dengan parameter nilai atribut
    public Toko(String item, String deskripsi, String merek, Integer harga) {
        
        // menginisialisasi nilai kedalam atribut
        this.item = item;
        this.deskripsi = deskripsi;
        this.merek = merek;
        this.harga = harga;

    }

}