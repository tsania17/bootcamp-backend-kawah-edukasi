# **INFORMASI PROJEK**

Bahasa Pemrograman: __*JAVA version 11.0.15*__

Framework : __*Quarkus version 2.12.3*__

DBMS : __*POSTGRESQL*__

IDE : __*IntelliJ IDEA Community Edition, Visual Studio Code*__


> ### Dependencies
- Quarkus Hibernate Validator ([ referensi ](https://quarkus.io/guides/validation))
- Quarkus SmallRye OpenAPI ([ referensi ](https://quarkus.io/guides/openapi-swaggerui))
- Quarkus Hibernate ORM with Panache ([ referensi ](https://quarkus.io/guides/hibernate-orm-panache))
- Quarkus JDBC Driver - PostgrSQL 
- Quarkus RESTEasy Reactive Jackson


> ### Entitas Beserta Aturan Atribut
- Customer, atribut: 
    - id
    - cname *(wajib diisi)*
    - caddress
    - cstatus *(wajib diisi)*
    - cnumber *(angka minimal 620)*
- Employee, atribut:
    - id
    - ename *(wajib diisi)*
    - eaddress *(wajib diisi)*
    - jobfield *(wajib diisi)*
    - salary *(wajib diisi, angka minimal 100.000)*
- Product, atribut:
    - id
    - brand *(wajib diisi)*
    - category *(wajib diisi)*
    - details *(wajib diisi)*
    - price *(wajib diisi, angka minimal 0)*

> ### Relasi Antar Entitas
- Many To One : Product -> Customer
- Many To Many : Employee -> Customer

> ### Format Response 
- Status true *(Boolean)*
- Pesan berhasil *(String)*
- Data yang dikembalikan *(Object)*

> ### Projek Back-End Kawah Edukasi Batch III

![Gambar Bootcamp BE KE](be.jpeg) 