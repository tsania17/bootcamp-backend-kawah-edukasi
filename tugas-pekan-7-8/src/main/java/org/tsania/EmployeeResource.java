package org.tsania;

import org.tsania.models.Employee;
import org.tsania.utilities.TemplateResponse;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("employees")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmployeeResource {

    @GET
    public TemplateResponse getEmployees() {
        return new TemplateResponse(true, "SUCCESS", Employee.listAll());
    }

    @GET
    @Path("{id}")
    public TemplateResponse getEmployeeById(Long id) {
        Employee employee = Employee.findById(id);
        return new TemplateResponse(true, "SUCCESS", employee);
    }

    @POST
    @Transactional
    public TemplateResponse addEmployee(@Valid Employee employee) {
        employee.persist();
        return new TemplateResponse(true, "SUCCESS", employee);
    }

    @PUT
    @Path("{id}")
    @Transactional
    public TemplateResponse updateEmployee(@PathParam("id") Long id, @Valid Employee newEmployee) {
        Employee oldEmployee = Employee.findById(id);
        oldEmployee.ename = newEmployee.ename;
        oldEmployee.eaddress = newEmployee.eaddress;
        oldEmployee.jobfield = newEmployee.jobfield;
        oldEmployee.salary = newEmployee.salary;
        return new TemplateResponse(true, "SUCCESS", oldEmployee);
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public TemplateResponse deleteEmployee(@PathParam("id") Long id) {
        Employee employee = Employee.findById(id);
        Employee.deleteById(id);
        return new TemplateResponse(true, "SUCCESS", employee);
    }

    @PUT
    @Path("thr/{id}")
    @Transactional
    public TemplateResponse thr(@PathParam("id") Long id, @Valid Integer newSalary) {
        Employee.update("salary = ?1 where id = ?2", newSalary, id);
        Employee employee = Employee.findById(id);
        return new TemplateResponse(true, "SUCCESS", employee);
    }
}