package org.tsania.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "employees")
public class Employee extends PanacheEntityBase {

    @Id
    @SequenceGenerator(
            name = "employeeSequence",
            sequenceName = "employee_id_sequence",
            allocationSize = 1,
            initialValue = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employeeSequence")
    public Long id;
    @NotBlank(message = "Mohon masukkan nama pegawai")
    public String ename;
    @NotBlank(message = "Mohon masukkan alamat pegawai")
    public String eaddress;
    @NotBlank(message = "Mohon masukkan bagian pekerjaan pegawai")
    @Column(name = "job_field")
    public String jobfield;
    @NotNull(message = "Mohon masukkan gaji pegawai")
    @Min(message = "Minimal gaji pegawai adalah 100.000 rupiah", value = 100000)
    public Integer salary;

    @ManyToMany
    @JoinTable(
            name = "service",
            joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "id")
    )
    public List<Customer> customers;

    @JsonGetter
    public Long getId() {
        return this.id;
    }

    @JsonSetter
    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }
}
