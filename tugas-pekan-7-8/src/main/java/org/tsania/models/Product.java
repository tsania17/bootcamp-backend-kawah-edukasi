package org.tsania.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "products")
public class Product extends PanacheEntityBase {

    @Id
    @SequenceGenerator(
            name = "productSequence",
            sequenceName = "product_id_sequence",
            allocationSize = 1,
            initialValue = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "productSequence")
    public Long id;
    @NotBlank(message = "Mohon masukkan merek produk")
    public String brand;
    @NotBlank(message = "Mohon masukkan kategori produk")
    public String category;
    @NotBlank(message = "Mohon masukkan rincian jenis produk")
    public String details;
    @NotNull(message = "Mohon masukkan harga produk")
    @Min(message = "Harga tidak boleh bernilai negatif", value = 0)
    public Integer price;

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false)
    public Customer customer;

    @JsonIgnore
    public Customer getCustomer() {
        return customer;
    }

    @JsonGetter
    public Long getId() {
        return this.id;
    }

    @JsonSetter
    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }
}
