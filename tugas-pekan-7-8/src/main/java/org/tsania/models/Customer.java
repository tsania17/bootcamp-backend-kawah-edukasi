package org.tsania.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(name = "customers")
public class Customer extends PanacheEntityBase {

    @Id
    @SequenceGenerator(
            name = "customerSequence",
            sequenceName = "customer_id_sequence",
            allocationSize = 1,
            initialValue = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customerSequence")
    public Long id;
    @NotBlank(message = "Mohon masukkan nama customer")
    public String cname;
    public String caddress;
    @NotBlank(message = "Mohon masukkan status customer (gold member/platinum member/new member)")
    public String cstatus;
    @Min(message = "Mohon masukkan nomor telpon dengan benar (diawali dengan 62..)", value = 620)
    public Integer cnumber;

    @OneToMany(mappedBy = "customer")
    public List<Product> products;

    @JsonGetter
    public Long getId() {
        return this.id;
    }

    @JsonSetter
    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }
}
