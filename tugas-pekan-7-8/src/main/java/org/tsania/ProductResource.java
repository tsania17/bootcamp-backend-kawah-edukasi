package org.tsania;

import org.tsania.models.Product;
import org.tsania.utilities.TemplateResponse;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("products")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductResource {

    @GET
    public TemplateResponse getProducts() {
        return new TemplateResponse(true, "SUCCESS", Product.listAll());
    }

    @GET
    @Path("{id}")
    public TemplateResponse getProductById(Long id) {
        Product product = Product.findById(id);
        return new TemplateResponse(true, "SUCCESS", product);
    }

    @POST
    @Transactional
    public TemplateResponse addProduct(@Valid Product product) {
        product.persist();
        return new TemplateResponse(true, "SUCCESS", product);
    }

    @PUT
    @Path("{id}")
    @Transactional
    public TemplateResponse updateProduct(@PathParam("id") Long id, @Valid Product newProduct) {
        Product oldProduct = Product.findById(id);
        oldProduct.brand = newProduct.brand;
        oldProduct.category = newProduct.category;
        oldProduct.details = newProduct.details;
        oldProduct.price = newProduct.price;
        return new TemplateResponse(true, "SUCCESS", oldProduct);
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public TemplateResponse deleteProduct(@PathParam("id") Long id) {
        Product product = Product.findById(id);
        Product.deleteById(id);
        return new TemplateResponse(true, "SUCCESS", product);
    }

    @PUT
    @Path("promo/{id}")
    @Transactional
    public TemplateResponse promo(@PathParam("id") Long id, @Valid Integer newPrice) {
        Product.update("price = ?1 where id = ?2", newPrice, id);
        return new TemplateResponse(true, "SUCCESS", Product.findById(id));
    }
}
