package org.tsania;

import org.tsania.models.Customer;
import org.tsania.models.Product;
import org.tsania.utilities.TemplateResponse;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("customers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerResource {

    @GET
    public TemplateResponse getCustomers() {
        return new TemplateResponse (true, "SUCCESS", Customer.listAll());
    }

    @GET
    @Path("{id}")
    public TemplateResponse getCustomerById(Long id) {
        return new TemplateResponse(true, "SUCCESS", Customer.findById(id));
    }

    @POST
    @Transactional
    public TemplateResponse addCustomer(@Valid Customer customer) {
        customer.persist();
        List<Product> products = customer.products;
        for (Product product : products) {
            product.customer = customer;
            product.persist();
        }
        return new TemplateResponse(true, "SUCCESS", customer);
    }

    @PUT
    @Path("{id}")
    @Transactional
    public TemplateResponse updateCustomer(@PathParam("id") Long id, @Valid Customer newCustomer) {
        Customer oldCustomer = Customer.findById(id);
        oldCustomer.cname = newCustomer.cname;
        oldCustomer.caddress = newCustomer.caddress;
        oldCustomer.cstatus = newCustomer.cstatus;
        oldCustomer.cnumber = newCustomer.cnumber;
        return new TemplateResponse(true, "SUCCESS", oldCustomer);
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public TemplateResponse deleteCustomer(@PathParam("id") Long id) {
        Customer customer = Customer.findById(id);
        Customer.deleteById(id);
        return new TemplateResponse(true, "SUCCESS", customer);
    }

    @PUT
    @Path("membership/{id}")
    @Transactional
    public TemplateResponse membership(@PathParam("id") Long id, @Valid String newStatus) {
        Customer.update("cstatus = ?1 where id = ?2", newStatus, id);
        return new TemplateResponse(true, "SUCCESS", Customer.findById(id));
    }
}