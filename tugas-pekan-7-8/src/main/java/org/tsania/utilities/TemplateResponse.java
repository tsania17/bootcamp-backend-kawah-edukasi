package org.tsania.utilities;

public class TemplateResponse {
    public Boolean status;
    public String message;
    public Object data;

    public TemplateResponse() {}

    public TemplateResponse(Boolean status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
