import java.util.Scanner;

public class TugasPekan1 {
    public static void main(String[] args) {

        // User menginput pilihan menu yang ditampilkan oleh program
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n===================================================================================================================================================");
        System.out.println("Welcome To My Program!!! Berikut menu program disini..");
        System.out.println("1. Identitas\n2. Kalkulator\n3. Perbandingan");
        System.out.print("Pilih Menu (1/2/3) : ");
        int inputanUser = scanner.nextInt();

        // Menjalankan program sesuai inputan user 
        switch(inputanUser){
            // Jika user menginput pilihan 1, yakni menu "identitas" maka akan mencetak identitas pribadi saya ke layar
            case 1:
                System.out.println("============================================================== IDENTITAS ==========================================================================");
                System.out.println("Nama\t\t\t\t\t: \nTsania Ursila Razani\n");
                System.out.println("Alasan menekuni Back-End Developer\t: \nSangat menyukai coding terutama yang berhubungan dengan logika, serta job oppurtunity yang menjanjikan di bidang ini\n");
                System.out.println("Ekspektasi mengikuti bootcamp ini\t: \nIngin mengembangkan skill di bidang Back-End terutama yang bermanfaat di dunia kerja, dan tentunya ingin mendapat job opportunity dari sini");
                break;
            // Jika user menginput pilihan 2, yakni menu "kalkulator" maka akan mencetak hasil sesuai operasi 2 bilangan yang diinputkan
            case 2:
                System.out.println("============================================================= KALKULATOR ==========================================================================");
                // User menginput pilihan menu yang ditampilkan oleh program
                System.out.println("1. Penjumlahan\n2. Pengurangan\n3. Perkalian\n4. Pembagian\n5. Sisa Bagi");
                System.out.print("Pilih operasi untuk kalkulator (1/2/3/4/5): ");
                int inputanKalkulator = scanner.nextInt();
                // User menginput angka yang akan dihitung sesuai operasi kalkulator yang dipilih
                System.out.println("Masukkan 2 bilangan integer yang akan di operasikan");
                System.out.print("> angka pertama\t: ");
                int angka1 = scanner.nextInt();
                System.out.print("> angka kedua\t: ");
                int angka2 = scanner.nextInt();
                // Menginisialisasi variabel untuk menampung hasil operasi
                int hasil;
                // Menjalankan program sesuai inputan user 
                switch(inputanKalkulator){
                    // Jika user menginput pilihan 1, yakni menu "penjumlahan" maka 2 bilangan akan di jumlahkan lalu dicetak ke layar
                    case 1:
                        hasil = angka1 + angka2;
                        System.out.println("Hasil Penjumlahan => " + angka1 + " + " + angka2 + " = " + hasil);
                        break;
                    // Jika user menginput pilihan 2, yakni menu "pengurangan" maka 2 bilangan akan di kurangi lalu dicetak ke layar
                    case 2:
                        hasil = angka1 - angka2;
                        System.out.println("Hasil Pengurangan => " + angka1 + " - " + angka2 + " = " + hasil);
                        break;
                    // Jika user menginput pilihan 3, yakni menu "perkalian" maka 2 bilangan akan di kalikan lalu dicetak ke layar
                    case 3:
                        hasil = angka1 * angka2;
                        System.out.println("Hasil Perkalian => " + angka1 + " * " + angka2 + " = " + hasil);
                        break;
                    // Jika user menginput pilihan 4, yakni menu "pembagian" maka 2 bilangan akan di bagi lalu dicetak ke layar
                    case 4:
                        hasil = angka1 / angka2;
                        System.out.println("Hasil Pembagian => " + angka1 + " : " + angka2 + " = " + hasil);
                        break;
                    // Jika user menginput pilihan 5, yakni menu "sisa bagi" maka 2 bilangan akan di modulus lalu dicetak ke layar
                    case 5:
                        hasil = angka1 % angka2;
                        System.out.println("Hasil Sisa Bagi => " + angka1 + " % " + angka2 + " = " + hasil);
                        break;
                    // Jika user menginput nilai diluar pilhan menu 1-5, maka akan mencetak kalimat berikut ke layar
                    default:
                        System.out.println("Pilihan operasi bilangan antara " + angka1 + " dan " + angka2 + " yang anda pilih tidak ada di menu kalkulator :(");
                }
                break;
            // Jika user menginput pilihan 3, yakni menu "perbandingan" maka akan mencetak hasil perbadingan 2 bilangan yang diinputkan
            case 3:
                System.out.println("============================================================ PERBANDINGAN =========================================================================");
                // User menginput angka yang akan dihitung sesuai operasi kalkulator yang dipilih
                System.out.println("Masukkan 2 bilangan integer yang akan di bandingkan");
                System.out.print("> angka pertama\t: ");
                int bilangan1 = scanner.nextInt();
                System.out.print("> angka kedua\t: ");
                int bilangan2 = scanner.nextInt();
                // Menjalankan program sesuai inputan user dengan menggunakan perbandingan ">", "<", dan "=="
                if(bilangan1 > bilangan2)
                    System.out.println("Hasil Perbandingan => " + bilangan1 + " lebih besar dari " + bilangan2);
                else if(bilangan1 < bilangan2)
                    System.out.println("Hasil Perbandingan => " + bilangan1 + " lebih kecil dari " + bilangan2);
                else if(bilangan1 == bilangan2)
                    System.out.println("Hasil Perbandingan => " + bilangan1 + " sama dengan " + bilangan2);
                break;
            // Jika user menginput nilai diluar pilhan menu 1-3, maka akan mencetak kalimat berikut ke layar
            default:
                System.out.println("Pilihan Tidak Tersedia..");
        }
        System.out.println("===================================================================================================================================================\n");

        scanner.close();

    }
}