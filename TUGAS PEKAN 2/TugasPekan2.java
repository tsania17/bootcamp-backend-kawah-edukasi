import java.util.Scanner;

public class TugasPekan2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int lanjut = 1;
        do{
            // User menginput pilihan menu yang ditampilkan oleh program
            System.out.println("Welcome To My Program!!! Berikut menu program disini..");
            System.out.println("1. Soal Pertama\n2. Soal Kedua\n3. Soal Ketiga");
            System.out.print("Pilih Menu (1/2/3): ");
            int inputanUser = scanner.nextInt();

            // Menjalankan program sesuai inputan user 
            switch(inputanUser){
                case 1:
                    System.out.println("Method yang menerima 1 parameter yang bertipe data angka integer yang mengembalikan nilai absolute (mutlak) dari angka yang dimasukkan ke parameter method tersebut");
                    System.out.println("==================================");
                    System.out.print("Masukkan Bilangan Integer: ");
                    int angka = scanner.nextInt();
                    int hasilCase1 = methodBilMutlak(angka);
                    System.out.println("Hasil => " + hasilCase1);
                    System.out.println("==================================\n");
                    break;
                case 2:
                    System.out.println("Method yang menerima 1 parameter yang bertipe array dari tipe data angka integer yang mengembalikan nilai yang terbesar pada array yang dimasukkan ke parameter method tersebut");
                    System.out.println("==================================");
                    System.out.print("Masukkan panjang array maks 10\t: ");
                    int panjangArray = scanner.nextInt();
                    System.out.print("Masukkan " + panjangArray + " angka array\t\t: ");
                    int [] array = new int [10];
                    for(int i=0; i<panjangArray; i++)  
                    {  
                        array[i] = scanner.nextInt(); 
                    }  
                    int hasilCase2 = methodBilTerbesar(array);
                    System.out.println("Hasil => " + hasilCase2);
                    System.out.println("==================================\n");
                    break;
                case 3:
                    System.out.println("Method yang menerima 2 parameter yang bertipe data angka integer yang mengembalikan hasil perpangkatan parameter pertama oleh parameter kedua");
                    System.out.println("==================================");
                    System.out.print("Masukkan bilangan\t: ");
                    int bilangan = scanner.nextInt();
                    System.out.print("Masukkan pangkat\t: ");
                    int pangkat = scanner.nextInt();
                    int hasilCase3 = methodBilPangkat(bilangan, pangkat);
                    System.out.println("Hasil => " + hasilCase3);
                    System.out.println("==================================\n");
                    break;
            }

            // User menginput pilihan menu untuk melanjutkan program atau keluar dari program berdasar pilihan yang ada
            System.out.print("1. Lanjut\n2. Keluar\nPilih Menu (1/2) : ");
            lanjut = scanner.nextInt();
        }while(lanjut == 1);

    }

    public static int methodBilMutlak(int x){
        if(x < 0)
            return -x;
        else 
            return x;        
    }

    public static int methodBilTerbesar(int [] x){
        int terbesar = x[0];
        for(int i = 1; i < x.length; i++){
            if(x[i] > terbesar)
                terbesar = x[i];
        }       
        return terbesar; 
    }

    public static int methodBilPangkat(int x, int y){
        int hasilPangkat = x;
        for(int i=y; i > 1; i--){
            hasilPangkat = hasilPangkat * x;
        }
        return hasilPangkat;
    }
}